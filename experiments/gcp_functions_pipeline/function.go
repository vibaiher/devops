package helloworld

import (
	"fmt"
	"net/http"
)

// HelloHTTP handles GCP function requests
func HelloHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello world!")
}
