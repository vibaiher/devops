# Pipeline for Google Cloud Platform Functions

This experiment is for trying to deploy a `function` into the Google Cloud Platform (GCP), using a pipeline.

## How to generate a `function` manually

Manual steps:

- Go to: [https://console.cloud.google.com/functions](https://console.cloud.google.com/functions)
- Create a GCP project (if not exists)
  - Options:
    - Name
    - Organization (optional)
- Create a function:
  - Options:
    - Name
    - Memory: from 128 MB to 2 GB
    - Trigger: from HTTP to Cloud Pub/Sub, etc..
    - Auth: public or private
    - Source code: inline editor, zip, repository in GCP...
    - Runtime: Go (1.11), Node.js (6, 8, 10), Python (3.7)
    - Handler
  - Advanced:
    - Region
    - Timeout
    - Max. instances
    - Account/project
    - VPC
    - Environment variables
- After creating, console goes to list of functions, and a spinner appears at the new one until it is healthy
- After enabling it, it cannot be paused, it only can be deleted.

`function` created: https://europe-west1-devops-rocks.cloudfunctions.net/hello-world

## How to generate a `function` programmatically

### Useful links

- [https://cloud.google.com/functions/docs/writing/specifying-dependencies-go](https://cloud.google.com/functions/docs/writing/specifying-dependencies-go)
- [https://cloud.google.com/functions/docs/writing/#functions-writing-file-structuring-go](https://cloud.google.com/functions/docs/writing/#functions-writing-file-structuring-go)
- [https://cloud.google.com/functions/docs/concepts/go-runtime](https://cloud.google.com/functions/docs/concepts/go-runtime)

## How to generate an automated test to validate if a `function` is up & running
